export type CakeType = {
  id: number;
  name: string;
  comment: string;
  imageUrl: string;
  yumFactor: number;
}
