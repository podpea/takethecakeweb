import {gql} from 'apollo-boost';

export default gql`
mutation ($cake: CakeInput!) {
  createCake(cake: $cake){
    id
    name
    comment
    imageUrl
    yumFactor
  }
}
`;
