import {gql} from 'apollo-boost';

export default gql`
mutation ($cakeId: Int!, $yumFactor: Int!) {
  addCakeYumFactor(cakeId: $cakeId, yumFactor: $yumFactor){
    id
    name
    comment
    imageUrl
    yumFactor
  }
}
`;
