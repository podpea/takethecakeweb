import {gql} from 'apollo-boost';

export default gql`
mutation ($cake: CakeInput!) {
  removeCake(cake: $cake)
}
`;
