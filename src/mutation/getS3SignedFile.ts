import {gql} from 'apollo-boost';

export default gql`
mutation ($file: S3SignFileInput!){
  getS3SignedUrl(file: $file){
    url
    signedRequest
  }
}
`;
