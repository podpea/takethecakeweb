import {gql} from 'apollo-boost';

export default gql`
query {
  cakes{
    id
    name
    comment
    imageUrl
    yumFactor
  }
}
`;
