import {gql} from 'apollo-boost';

export default gql`
subscription {
  cakeAdded{
    id
    name
    comment
    imageUrl
    yumFactor
  }
}
`;
