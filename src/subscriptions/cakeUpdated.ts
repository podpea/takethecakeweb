import {gql} from 'apollo-boost';

export default gql`
subscription {
  cakeUpdated{
    id
    name
    comment
    imageUrl
    yumFactor
  }
}
`;
