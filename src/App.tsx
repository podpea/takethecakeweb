import * as React from 'react';

import Nav from './components/Navigation';
import CakesListContainer from './components/cakes/CakesListContainer';
import CakeInputModal from './components/cakes/CakeInputModal';

const App: React.FC = () => {
  const [createModalOpen, setCreateModalOpen] = React.useState(false);

  return (
    <div>
      <Nav onCreateClick={() => setCreateModalOpen(true)} />
      <div className="pt-5">
        <CakesListContainer onAddCake={() => setCreateModalOpen(true)} />
      </div>
      <CakeInputModal
        isOpen={createModalOpen}
        toggle={() => setCreateModalOpen(!createModalOpen)}
        onSave={() => setCreateModalOpen(false)}
      />
    </div>
  );
};

export default App;
