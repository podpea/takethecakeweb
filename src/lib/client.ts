// @ts-nocheck
import {split, HttpLink} from '@apollo/client';
import {getMainDefinition} from '@apollo/client/utilities';
import {WebSocketLink} from '@apollo/client/link/ws';
import {ApolloClient, InMemoryCache} from '@apollo/client';
import {setContext} from '@apollo/client/link/context';
import uuid from 'react-uuid';

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_API_HTTP_URL,
});

const wsLink = new WebSocketLink({
  uri: process.env.REACT_APP_API_WS_URL,
  options: {
    reconnect: true,
  },
});

const authLink = setContext((_, {headers}) => {
  if (!localStorage.getItem('uid')) {
    localStorage.setItem('uid', uuid());
  }

  const uid = localStorage.getItem('uid');

  return {
    headers: {
      ...headers,
      uid,
    },
  };
});

const link = split(
    ({query}) => {
      const definition = getMainDefinition(query);
      return (
        definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
      );
    },
    wsLink,
    httpLink,
);

const client = new ApolloClient({
  link: authLink.concat(link),
  cache: new InMemoryCache(),
});

export default client;

