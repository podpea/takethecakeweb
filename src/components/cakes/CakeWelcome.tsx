import * as React from 'react';
import {Jumbotron, Button} from 'reactstrap';

interface Props {
  onAddCake: Function;
}

const CakeWelcome: React.FC<Props> = ({onAddCake}: Props) => {
  return (
    <Jumbotron className="mt-4">
      <h1 className="display-3">Welcome!</h1>
      <p className="lead">
        Show off all of your beautiful cake pics.
      </p>
      <hr className="my-2" />
      <p>
        Use the Add Cake button to upload your cake pictures
        and allow people around the globe to rate them.
      </p>
      <p className="lead">
        <Button color="success" onClick={() => onAddCake()}>
          Add Cake
        </Button>
      </p>
    </Jumbotron>
  );
};

export default CakeWelcome;
