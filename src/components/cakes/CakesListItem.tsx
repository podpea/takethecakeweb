import * as React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
} from 'reactstrap';
import Rating from 'react-rating';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {useMutation} from '@apollo/client';

import ConfirmModal from '../ConfirmModal';

import {CakeType} from '../../types';
import rateCakeMutation from '../../mutation/rateCake';
import removeCakeMutation from '../../mutation/removeCake';
import cakeQuery from '../../queries/cakes';

interface Props {
  cake: CakeType;
}

const CakesListItem: React.FC<Props> = ({cake}: Props) => {
  const [confirmModalOpen, setConfirmModalOpen] = React.useState(false);
  const [rateCake] = useMutation(rateCakeMutation);
  const [removeCake, {loading}] = useMutation(removeCakeMutation);
  return (
    <div>
      <Card className="mt-4">
        <CardBody>
          <div className="d-flex flex-row justify-content-between">
            <CardTitle tag="h5">{cake.name}</CardTitle>
            <Button
              size="sm"
              color="link"
              onClick={() => {
                setConfirmModalOpen(true);
              }}
            >
              <FontAwesomeIcon
                icon={['far', 'trash-alt']}
                color="black"
              />
            </Button>
          </div>
        </CardBody>
        <img
          style={{
            width: '100%',
            maxWidth: '700px',
            height: '400px',
            objectFit: 'cover',
          }} src={cake.imageUrl}
          alt="Card image cap"
        />
        <CardBody>
          <CardText>{cake.comment}</CardText>
          <Rating
            stop={5}
            initialRating={cake.yumFactor}
            emptySymbol={<FontAwesomeIcon icon={['far', 'star']} />}
            fullSymbol={
              <FontAwesomeIcon icon={['fas', 'star']} color="#FFD700" />
            }
            onChange={async (value) => {
              try {
                await rateCake({
                  variables: {
                    cakeId: cake.id,
                    yumFactor: value,
                  },
                  optimisticResponse: {
                    __typename: 'Mutation',
                    addCakeYumFactor: {
                      __typename: 'CakeType',
                      id: cake.id,
                      name: cake.name,
                      comment: cake.comment,
                      imageUrl: cake.imageUrl,
                      yumFactor: value,
                    },
                  },
                });
              } catch (err) {
                alert(err.message);
              }
            }}
          />
        </CardBody>
      </Card>
      <ConfirmModal
        isOpen={confirmModalOpen}
        toggle={() => setConfirmModalOpen(!confirmModalOpen)}
        message="Are you sure you want to delete the cake?"
        loading={loading}
        onConfirm={async () => {
          try {
            await removeCake({
              variables: {
                cake: {
                  id: cake.id,
                },
              },
              update: (proxy, {data: {removeCake}}) => {
                const data: any = proxy.readQuery({query: cakeQuery});

                proxy.writeQuery({
                  query: cakeQuery,
                  data: {
                    ...data,
                    cakes: [
                      ...data.cakes.filter(
                          (cake: CakeType) => cake.id !== removeCake,
                      ),
                    ],
                  },
                });
              },
            });
          } catch (err) {
            alert(err.message);
          }
        }}
      />
    </div>
  );
};

export default CakesListItem;
