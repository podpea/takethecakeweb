import * as React from 'react';

import CakesListItem from './CakesListItem';

import type {CakeType} from '../../types/';

interface Props {
  data: Array<CakeType>;
  subscribeToCakeAdded: Function;
  subscribeToCakeUpdated: Function;
  subscribeToCakeRemoved: Function;
}

const CakesList: React.FC<Props> = ({
  data,
  subscribeToCakeAdded,
  subscribeToCakeUpdated,
  subscribeToCakeRemoved,
}: Props) => {
  React.useEffect(() => {
    subscribeToCakeAdded();
    subscribeToCakeUpdated();
    subscribeToCakeRemoved();
  }, []);

  return (
    <div className="d-flex flex-column">
      {data.map((cake) => (
        <CakesListItem key={cake.id} cake={cake} />
      ))}
    </div>
  );
};

export default CakesList;
