import * as React from 'react';
import {useQuery} from '@apollo/client';

import CakeWelcome from './CakeWelcome';
import CakesList from './CakesList';
import Loading from '../Loading';

import cakesQuery from '../../queries/cakes';
import cakeAddedSubscription from '../../subscriptions/cakeAdded';
import cakeUpdatedSubscription from '../../subscriptions/cakeUpdated';
import cakeRemovedSubscription from '../../subscriptions/cakeRemoved';

import type {CakeType} from '../../types';

interface Props {
  onAddCake: Function;
}

const CakesListContainer: React.FC<Props> = ({onAddCake}: Props) => {
  const {data, loading, subscribeToMore} = useQuery(cakesQuery);

  if (loading) {
    return <Loading />;
  }

  return (
    <div className="d-flex justify-content-center mt-3 pb-3">
      {
        data.cakes.length === 0 ?

        <CakeWelcome onAddCake={onAddCake}/> :

        <CakesList
          data={data.cakes}
          subscribeToCakeAdded={() => {
            subscribeToMore({
              document: cakeAddedSubscription,
              updateQuery: (prev, {subscriptionData}) => {
                if (!subscriptionData.data) return prev;
                const {cakeAdded} = subscriptionData.data;

                if (prev.cakes.some(
                    (cake: CakeType) => cake.id === cakeAdded.id)
                ) {
                  return prev;
                }

                return {
                  ...prev,
                  cakes: [
                    cakeAdded,
                    ...prev.cakes,
                  ],
                };
              },
            });
          }}
          subscribeToCakeUpdated={() => {
            subscribeToMore({
              document: cakeUpdatedSubscription,
              updateQuery: (prev, {subscriptionData}) => {
                if (!subscriptionData.data) return prev;
                const {cakeUpdated} = subscriptionData.data;

                const index = prev.cakes.findIndex((cake: CakeType) => {
                  return cake.id === cakeUpdated.id;
                });

                return {
                  ...prev,
                  cakes: [
                    ...prev.cakes.slice(0, index),
                    cakeUpdated,
                    ...prev.cakes.slice(index+1),
                  ],
                };
              },
            });
          }}
          subscribeToCakeRemoved={() => {
            subscribeToMore({
              document: cakeRemovedSubscription,
              updateQuery: (prev, {subscriptionData}) => {
                if (!subscriptionData.data) return prev;
                const {cakeRemoved} = subscriptionData.data;

                return {
                  ...prev,
                  cakes: [
                    ...prev.cakes.filter(
                        (cake: CakeType) => cake.id !== cakeRemoved,
                    ),
                  ],
                };
              },
            });
          }}
        />
      }
    </div>
  );
};

export default CakesListContainer;
