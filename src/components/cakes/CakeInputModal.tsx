import * as React from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
  Form,
  FormGroup,
  FormFeedback,
  Label,
  Input,
} from 'reactstrap';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {useMutation} from '@apollo/client';
import axios from 'axios';

import getS3SignedUrlMutation from '../../mutation/getS3SignedFile';
import createCakeMutation from '../../mutation/createCake';
import cakeQuery from '../../queries/cakes';

const schema = Yup.object().shape({
  name: Yup.string().max(30).required(),
  comment: Yup.string().max(200).required(),
  photo: Yup.mixed().required(),
});

interface Props {
  isOpen: boolean;
  toggle: Function;
  onSave: Function;
}

const CakeInputModal: React.FC<Props> = ({
  isOpen,
  toggle,
  onSave,
}: Props) => {
  const [saving, setSaving] = React.useState(false);
  const [getS3SignedFile] = useMutation(getS3SignedUrlMutation);
  const [createCake] = useMutation(createCakeMutation);

  const formik = useFormik({
    initialValues: {
      name: '',
      comment: '',
      photo: undefined,
    },
    validationSchema: schema,
    onSubmit: async (values: any) => {
      setSaving(true);
      try {
        const {data: {getS3SignedUrl}} = await getS3SignedFile({
          variables: {
            file: {
              name: values?.photo?.name,
              type: values?.photo?.type,
            },
          },
        });

        const options = {
          headers: {
            'Content-Type': values?.photo?.type,
          },
        };

        await axios.put(
            getS3SignedUrl.signedRequest,
            values?.photo,
            options,
        );

        const imageUrl = encodeURI(getS3SignedUrl.url);

        await createCake({
          variables: {
            cake: {
              name: values.name,
              comment: values.comment,
              imageUrl,
            },
          },
          update: (proxy, createCakeRes) => {
            const data: any = proxy.readQuery({query: cakeQuery});

            if (data.cakes.some(
                (cake: any) => cake.id === createCakeRes.data.createCake.id)
            ) {
              return data;
            }

            proxy.writeQuery({
              query: cakeQuery,
              data: {
                ...data,
                cakes: [
                  createCakeRes.data.createCake,
                  ...data.cakes,
                ],
              },
            });
          },
        });
        onSave();
        formik.resetForm();
      } catch (err) {
        alert(err.message);
      } finally {
        setSaving(false);
      }
    },
  });

  return (
    <Modal
      isOpen={isOpen}
      toggle={() => {
        toggle();
        formik.resetForm();
      }}>
      <Form onSubmit={(e) => {
        e.preventDefault();
        formik.handleSubmit(e);
      }}>
        <ModalHeader>Add Cake</ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label size="sm" for="name">Name *</Label>
            <Input
              bsSize="sm"
              valid={formik.touched.name && !formik.errors?.name}
              invalid={formik.touched.name && !!formik.errors?.name}
              id="name"
              placeholder="Name of cake"
              {...formik.getFieldProps('name')}
            />
            <FormFeedback>{formik.errors.name}</FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label size="sm" for="comment">Comment *</Label>
            <Input
              bsSize="sm"
              valid={formik.touched.comment && !formik.errors.comment}
              invalid={formik.touched.comment && !!formik.errors.comment}
              type="textarea"
              id="comment"
              placeholder="Say something about the cake"
              {...formik.getFieldProps('comment')}
            />
            <FormFeedback>{formik.errors.comment}</FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label size="sm" for="photo">Photo *</Label>
            <Input
              valid={formik.touched.photo && !formik.errors.photo}
              invalid={formik.touched.photo && !!formik.errors.photo}
              bsSize="sm"
              type="file"
              id="file"
              name="file"
              accept="image/*"
              onChange={(evt) => {
                const {files} = evt.currentTarget;

                if (files && files?.length > 0) {
                  formik.setFieldValue('photo', files[0]);
                } else {
                  formik.setFieldValue('photo', undefined);
                }

                formik.setFieldTouched('photo', true);
              }}
            />
            <FormFeedback>{formik.errors.photo}</FormFeedback>
          </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button
            size="sm"
            color="secondary"
            onClick={() => toggle()}
          >Cancel</Button>
          <Button
            type="submit"
            size="sm"
            color="success"
            disabled={saving}
          >{(saving) ?
            'Saving...' :
            'Save Cake'}
          </Button>
        </ModalFooter>

      </Form>
    </Modal>
  );
};

export default CakeInputModal;
