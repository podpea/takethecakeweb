import React, {useState} from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  Button,
} from 'reactstrap';

interface Props {
  onCreateClick: Function;
}

const Navigation: React.FC<Props> = ({onCreateClick}: Props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md" fixed="top">
        <NavbarBrand href="/">Take the Cake</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar />
          <Button color="success" size="sm" onClick={() => onCreateClick()}>
            Add Cake
          </Button>
        </Collapse>
      </Navbar>
    </div>
  );
};

export default Navigation;
