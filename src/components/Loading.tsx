import * as React from 'react';

const Loading: React.FC = () => {
  return (
    <div className="d-flex justify-content-center mt-5">
      <h1>Loading...</h1>
    </div>
  );
};

export default Loading;
