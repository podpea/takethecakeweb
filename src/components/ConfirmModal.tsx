import * as React from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
} from 'reactstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

interface Props {
  isOpen: boolean;
  toggle: Function;
  onConfirm: Function;
  message: string;
  loading?: boolean;
}

const ConfirmModal: React.FC<Props> = ({
  isOpen,
  toggle,
  onConfirm,
  message,
  loading,
}: Props) => {
  return (
    <Modal
      isOpen={isOpen}
      toggle={() => {
        toggle();
      }}>
      <ModalHeader>Confirm</ModalHeader>
      <ModalBody>
        {message}
      </ModalBody>
      <ModalFooter>
        <Button
          size="sm"
          color="secondary"
          onClick={() => toggle()}
        >Cancel</Button>
        <Button
          type="submit"
          size="sm"
          color="success"
          disabled={loading}
          onClick={() => onConfirm()}
        >{(loading) ?
          <FontAwesomeIcon
            icon="spinner"
            spin
            color="black"
          /> :
            'Confirm'}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ConfirmModal;
