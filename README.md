# Take the Cake Web

## Requirements

- node 12.13
- AWS S3 with public bucket

---

## Installation

`npm install`

---

## Run the server

`npm start`

## Notes

- Ensure you have created a `.env` file in the route of the project. `.env.example` shows which variables need to be populated.